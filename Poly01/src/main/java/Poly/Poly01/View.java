package Poly.Poly01;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.*;



public class View extends JFrame {
	JPanel thePanel = new JPanel();
	JLabel firstPolynomLabel = new JLabel("Introduce first Polynom");
	JTextField poly1 = new JTextField("Introduce first Polynom here:", 20);
	JLabel secondPolynomLabel = new JLabel("Introduce second Polynom");
	JTextField poly2 = new JTextField("Introduce second Polynom here:", 20);
	JLabel labelResult = new JLabel("The result is");
	JTextField polyResult = new JTextField(40);
	JButton add = new JButton("Add");
	JButton sub = new JButton("Subtraction");
	JButton mul = new JButton("Multiplication");
	JButton inte = new JButton("Integration");
	JButton der = new JButton("Derivative");
	JButton div = new JButton("Division");
	JLabel labelInstructions = new JLabel("Please enter a polynom as follows (sign+-)constant(x^)degree. Example +1x^3-2xˆ2-4xˆ0 for the integration and derivative operation we will consider as input the first TextField" );

	public View(){
		this.setSize(500, 500);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Polynomial Calculator");
		
		
		this.add(thePanel);
		thePanel.add(firstPolynomLabel);
		thePanel.add(poly1);
		thePanel.add(secondPolynomLabel);
		thePanel.add(poly2);   
		thePanel.add(labelResult);
		thePanel.add(polyResult);
		thePanel.add(add);
		thePanel.add(sub);
		thePanel.add(mul);
		thePanel.add(inte);
		thePanel.add(der);
		thePanel.add(div);
		thePanel.add(labelInstructions);
		
		
		
		
		
		
		this.setVisible(true);
	}
	
	public char signNum(int x){
		if (x >= 0){
			return '+';
		}else{
			return '-';
		}
	}
	
	 public Polynom getPolynom(JTextField inputField) {  
		ArrayList<Monom> aux = new ArrayList<Monom>();
		String cop;
		String input = inputField.getText();
		
		Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
		Matcher matcher = pattern.matcher(input);
		while (matcher.find()) {
		    cop = matcher.group(0);
		  
		    if(cop.contains("xˆ")){
		    	if(cop.contains("-")){ 
		    		
			    	int indexSub = input.indexOf('x');
			    	int coef =(-1)*Integer.parseInt(cop.substring(1, indexSub));
			    	int degree = Integer.parseInt(cop.substring(indexSub+2, cop.length()));
			    	Monom a = new Monom(coef, degree);
			    	aux.add(a);
			    	
		    	}else if(cop.contains("+")){
		    		
		    		int indexSub = input.indexOf('x');
			    	int coef =Integer.parseInt(cop.substring(1, indexSub));
			    	int degree = Integer.parseInt(cop.substring(indexSub+2, cop.length()));
			    	Monom a = new Monom(coef, degree);
			    	aux.add(a);
			    	
			    	}else{
		    		
		    		int indexSub = input.indexOf('x');
			    	int coef =Integer.parseInt(cop.substring(0, indexSub));
			    	int degree = Integer.parseInt(cop.substring(indexSub+2, cop.length()));
			    	Monom a = new Monom(coef, degree);
			    	aux.add(a);
			    	
			    	}
		    }
		    
		    if(cop.contains("Xˆ")){
		    	if(cop.contains("-")){ 
		    		
			    	int indexSub = input.indexOf('X');
			    	int coef =(-1)*Integer.parseInt(cop.substring(1, indexSub));
			    	int degree = Integer.parseInt(cop.substring(indexSub+2, cop.length()));
			    	Monom a = new Monom(coef, degree);
			    	aux.add(a);
			    	
		    	}else if(cop.contains("+")){
		    		
		    		int indexSub = input.indexOf('X');
			    	int coef =Integer.parseInt(cop.substring(1, indexSub));
			    	int degree = Integer.parseInt(cop.substring(indexSub+2, cop.length()));
			    	Monom a = new Monom(coef, degree);
			    	aux.add(a);
			    	
			    	}else{
		    		
		    		int indexSub = input.indexOf('X');
			    	int coef =Integer.parseInt(cop.substring(0, indexSub));
			    	int degree = Integer.parseInt(cop.substring(indexSub+2, cop.length()));
			    	Monom a = new Monom(coef, degree);
			    	aux.add(a);
			    	
			    	}
		    }
		}
	
		Polynom rez = new Polynom(aux);
		return rez;
		
		
		}

	 
	
	public void setResult(Polynom solution) { 
		String aux = "";
				
		for(int i =0; i < solution.getPoly().size(); i++)	{	
			aux = aux + signNum(solution.getPoly().get(i).getConstant()) + solution.getPoly().get(i).getConstant() + "*X^"  + solution.getPoly().get(i).getDegree();
		}
		
		polyResult.setText(aux);
	}
	
	public void setResult(String solution) {	
		polyResult.setText(solution);
	}
	
	
	public void setResultForIntegral(Polynom solution) { 
		String aux = "";
				
		for(int i =0; i < solution.getPoly().size(); i++)	{	
			aux = aux + signNum(solution.getPoly().get(i).getConstant()) + solution.getPoly().get(i).getConstant() + "/"  + solution.getPoly().get(i).getDegree() + "*X^"  + solution.getPoly().get(i).getDegree();
		}
		
		polyResult.setText(aux);
	}
	
	void addCalculationListener(ActionListener listenForAddButton){
		add.addActionListener(listenForAddButton);			
		};
		
	void subCalculationListener(ActionListener listenForSubButton){
		sub.addActionListener(listenForSubButton);			
		};
		
	void derivCalculationListener(ActionListener listenForDerivButton){
		der.addActionListener(listenForDerivButton);			
				};
	
	void integCalculationListener(ActionListener listenForIntegButton){
		inte.addActionListener(listenForIntegButton);			
				};
   void mulCalculationListener(ActionListener listenForMulButton){
					mul.addActionListener(listenForMulButton);			
							};
	void divCalculationListener(ActionListener listenForDivButton){
			div.addActionListener(listenForDivButton);			
	};	
		
	void displayErrorMessage(String errorMessage){
		JOptionPane.showMessageDialog(this, errorMessage);
	}
	
}

