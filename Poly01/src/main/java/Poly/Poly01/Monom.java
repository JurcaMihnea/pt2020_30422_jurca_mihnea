package Poly.Poly01;

import java.text.DecimalFormat;

public class Monom{
	
	private int constant;
	private int degree;
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + constant;
		result = prime * result + degree;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Monom other = (Monom) obj;
		if (constant != other.constant)
			return false;
		if (degree != other.degree)
			return false;
		return true;
	}

	public Monom(int constant, int degree){
		
		this.constant = constant;
		this.degree = degree;
		
	}
	
	public Monom(){
		
		this.constant = 0;
		this.degree = 0;
		
	}
	
	public Monom monomAdd(Monom monom){
		
		Monom result = new Monom();
		
		result.setDegree(monom.getDegree());
		result.setConstant(this.getConstant() + monom.getConstant());
		
		return result;
	}
	
	public Monom monomSub(Monom monom){
			
			Monom result = new Monom();
			
			result.setDegree(monom.getDegree());
			result.setConstant(this.getConstant() - monom.getConstant());
			
			return result;
		}
	
	public Monom monomMul(Monom monom){
		
		Monom result = new Monom();
		
		result.setDegree(this.getDegree() + monom.getDegree());
		result.setConstant( monom.getConstant() * this.getConstant());
		
		return result;
	}
	
	public Monom monomDeriv(){
			
			Monom result = new Monom();
			
			result.setConstant(this.getConstant() * this.getDegree());
			result.setDegree(this.getDegree() - 1);
				
			return result;
		}
	
	public Monom monomInt(){
		
		Monom result = new Monom();
		
		result.setDegree( this.getDegree() + 1);
		result.setConstant( this.getConstant());	
			
		return result;
	}
	
	
	public void setConstant(int constant) {
		this.constant = constant;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public int getDegree() {
		return degree;
	}

	public int getConstant() {
		return constant;
	}
	
	public String monomToString() {
		String result;
		
		result = this.constant + "x^" + this.degree;
		 
		return result;
  } 
	}
